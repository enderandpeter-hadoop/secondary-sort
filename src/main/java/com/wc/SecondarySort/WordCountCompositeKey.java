package com.wc.SecondarySort;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class WordCountCompositeKey implements WritableComparable<WordCountCompositeKey>{
	
	private IntWritable inputCount;
	
	public IntWritable getInputCount() {
		return inputCount;
	}

	public void setInputCount(IntWritable inputCount) {
		this.inputCount = inputCount;
	}

	public Text getInputKey() {
		return inputKey;
	}

	public void setInputKey(Text inputKey) {
		this.inputKey = inputKey;
	}

	private Text inputKey;
	
	public void readFields(DataInput arg0) throws IOException {
		inputCount.readFields(arg0);
		inputKey.readFields(arg0);
		
	}

	public void write(DataOutput arg0) throws IOException {
		inputCount.write(arg0);
		inputKey.write(arg0);
		
	}

	public int compareTo(WordCountCompositeKey arg0) {
		int value = inputKey.compareTo(arg0.inputKey);
		
		if(value != 0) {
			return -inputCount.compareTo(arg0.inputCount);
		}
		
		return value;
	}

	public WordCountCompositeKey() {
		inputCount = new IntWritable();
		inputKey = new Text();
	}

	public WordCountCompositeKey(Text inputKey, IntWritable inputCount) {
		this.inputCount = inputCount;
		this.inputKey = inputKey;
	}
	
	@Override
	public String toString() {

		return inputKey + " " + inputCount;
	}
}
