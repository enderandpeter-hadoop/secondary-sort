package com.wc.SecondarySort;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.HashPartitioner;
import org.apache.hadoop.mapreduce.Partitioner;

public class WordCountPartitioner extends Partitioner<WordCountCompositeKey, Text>{

	HashPartitioner<Text, Text> hashPartition = new HashPartitioner<Text, Text>();
	
	Text newKey = new Text();
	
	@Override
	public int getPartition(WordCountCompositeKey arg0, Text arg1, int arg2) {
		try {
			newKey.set(arg0.getInputKey());
			return hashPartition.getPartition(newKey, arg1, arg2);
		} catch(Exception e) {
			e.printStackTrace();
			return (int) (Math.random() * arg2);
		}
	}
}
