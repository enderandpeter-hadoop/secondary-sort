package com.wc.SecondarySort;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCountMapper extends Mapper<LongWritable, Text, WordCountCompositeKey, NullWritable> {
	
	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		StringTokenizer tokens = new StringTokenizer(value.toString(), "\n");
		
		while(tokens.hasMoreTokens()) {
			String[] vals = tokens.nextToken().split("\\t");
			context.write(new WordCountCompositeKey(new Text(vals[0]), new IntWritable(Integer.parseInt(vals[1]))), 
					NullWritable.get());
		}
	}
}
