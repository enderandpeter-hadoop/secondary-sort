package com.wc.SecondarySort;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class WordCountReducer 
	extends Reducer<WordCountCompositeKey, NullWritable, WordCountCompositeKey, NullWritable> {
	
	public void reduce(WordCountCompositeKey key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
		for(NullWritable vals : values) {
			context.write(key, NullWritable.get());
		}
	}
}
